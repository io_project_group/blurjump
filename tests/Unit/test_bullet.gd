extends "res://addons/gut/test.gd"

var Bullet := load("res://src/Enemies/Musketeer/Bullet.gd")

var _bullet = null

func before_each():
	_bullet = Bullet.new()

func after_each():
	_bullet.free()

func test_set_target_and_damage():
	_bullet.set_target_and_damage(Vector2(3, 4), Vector2(0,0), 30)
	
	assert_eq(_bullet.position, Vector2(0,0))
	assert_eq(_bullet.movement_vector, Vector2(0.6, 0.8))
	assert_eq(_bullet.damage, 30)
