# Blur Jump
#### Installation and running the game
1) [Download Godot Engine](https://godotengine.org/download) and unpack it. It's only several dozen MB - Godot is a really lightweight engine.
2) Clone this repository.
3) Open Godot and import project by selecting ```project.godot``` file from cloned repository. Then click Run to play or Edit to edit project files.
4) When editing, you can click F5 to run game in debug mode.
