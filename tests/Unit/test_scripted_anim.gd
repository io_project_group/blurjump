extends "res://addons/gut/test.gd"

var loaded = load("res://src/Levels/TestLevel.tscn")

var loaded_lvl

func before_each():
	loaded_lvl = loaded.instance()

func after_each():
	loaded_lvl.free()

# In this test we set character state to on flor, force jump and
# Check if variables that store time to specyfic accions have valid
# at the moment of jump and in next call - "simulated" frame after

func test_afterimage():
	var player = loaded_lvl.get_node("Player")
	var freeze_state = loaded_lvl.get_node("WorldState").get_node("Freeze")
	player.freeze_state = freeze_state
	player._ready()
	player.freeze_time(true)
	player.handle_animation(0.1) 
	assert_true(player.get_node("AfterImage").get_node("Img1").visible == true,
			"AfterImage animation failed")
