extends Control

func _ready():
	visible = false

func change_state():
	var new_pause_state = not get_tree().paused
	get_tree().paused = new_pause_state
	visible = new_pause_state
	get_child(1).active = not get_child(1).active

func _input(event):
	if event.is_action_pressed("pause"):
		change_state()
