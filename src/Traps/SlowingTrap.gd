extends Area2D

export var SLOW_FACTOR = 2

func _on_trap_entered(actor: Actor) -> void:
	if actor.get_class() == 'Player':
		actor.is_in_water=true
		actor.SPEED_MAX /= SLOW_FACTOR
		actor.JUMP_SPEED /= SLOW_FACTOR

func _on_trap_exited(actor: Actor) -> void:
	if actor.get_class() == 'Player':
		actor.is_in_water=false
		actor.SPEED_MAX *= SLOW_FACTOR
		actor.JUMP_SPEED *= SLOW_FACTOR
