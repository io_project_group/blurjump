extends "res://addons/gut/test.gd"

var loaded_scene = load("res://src/Player/Player.tscn")\

var player_scene

func before_each():
	player_scene = loaded_scene.instance()

func after_each():
	player_scene.free()

# In this test we set character state to on flor, force jump and
# Check if variables that store time to specyfic accions have valid
# at the moment of jump and in next call - "simulated" frame after

func test_movement_related_anim():
	var character = player_scene.get_node(".")
	character.set_physics_process(false)
	character._ready()
	#set state on floor in player
	character.on_floor = true
	
	character._ready()
	
	character.player_velocity.x = 0
	character.player_velocity.y = 0
	character.handle_move(0.1, 1) 
	character.handle_jump(0.1, true)
	
	#test for proper display of char states in jump
	character.player_velocity.y = character.JMP_STATES[0]-1
	character.handle_animation(0) 
	assert_true(player_scene.get_node("HeroSprite").frame == 1,
			"Bad Frame in in jump frame 1")
			
	character.player_velocity.y = character.JMP_STATES[1]-1
	character.handle_animation(0) 
	assert_true(player_scene.get_node("HeroSprite").frame == 2,
			"Bad Frame in in jump frame 2")
	
	character.player_velocity.y = character.JMP_STATES[2]-1
	character.handle_animation(0) 
	assert_true(player_scene.get_node("HeroSprite").frame == 3,
			"Bad Frame in in jump frame 3")
	
	character.player_velocity.y = character.JMP_STATES[3]-1
	character.handle_animation(0) 
	assert_true(player_scene.get_node("HeroSprite").frame == 4,
			"Bad Frame in in jump frame 4")
	 
	character.player_velocity.y = character.JMP_STATES[4]-1
	character.handle_animation(0) 
	assert_true(player_scene.get_node("HeroSprite").frame == 5,
			"Bad Frame in in jump frame 5")
			
	character.player_velocity.y = character.JMP_STATES[4]+1
	character.handle_animation(0) 
	assert_true(player_scene.get_node("HeroSprite").frame == 6,
			"Bad Frame in in jump frame 4")
