class_name Actor
extends KinematicBody2D

# Number of health points of character
export var HP = 100

#Number of energy points
export var MP = 100

onready var current_hp = HP
onready var current_mp = MP
#flag for dmg anim - set by take_damege unset by native local dmg anim handler
onready var dmg_anim = false

# Variable used by trap to check if node is still in its area
var is_taking_damage = false
var is_in_water = false
func take_damage(damage: int) -> void:
	dmg_anim = true
	print(name + " takes damage: " + String(damage))
	current_hp -= damage
	if current_hp <= 0:
		die()

func die() -> void:
	pass

