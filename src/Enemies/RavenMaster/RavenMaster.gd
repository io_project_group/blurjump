extends Actor

class_name RavenMaster

export var MAX_RAVENS_NUM = 5
export var RAVEN_SPAWN_COOLDOWN = 5.0
export var RAVEN_SPAWN_RELATIVE_POSITION = Vector2(10, -8)

onready var disabled = false
onready var ravens_num = 0
onready var cooldown_left = 0
onready var direction = -1 # default is facing is left side
var enemy_id
onready var anim_timer = 0
onready var anim_to_display = "Iddle"
onready var ANIM_RAVEN_SPAWN_TIME = 0.9
onready var ANIM_DEATH_TIME = 1.4
onready var anim_started = false
onready var anim_death = false
onready var death_lock = false

func _physics_process(delta: float) -> void:
	if(death_lock == false):
		if (disabled):
			return
		change_direction()
		reduce_cooldown(delta)
		if (cooldown_left <= 0 and ravens_num != MAX_RAVENS_NUM):
			spawn_raven()
	handle_anim()
	anim_timer -= delta
 
func handle_anim():
	if(anim_to_display != "Iddle"): 
		if(anim_started == false):
			if(anim_to_display == "SpawnRaven"):
				anim_timer = ANIM_RAVEN_SPAWN_TIME
				$RavenMasterSprite.play("SpawnRaven")
			if(anim_to_display == "Death"):
				anim_timer = ANIM_DEATH_TIME
				$RavenMasterSprite.play("Death")
			anim_started = true
		else:
			if(anim_timer <= 0):
				if(anim_to_display == "SpawnRaven"):
					$RavenMasterSprite.play("Iddle")
					anim_started = false
					anim_to_display = "Iddle"
				if(anim_to_display == "Death"):
					anim_death = true
					die()
			

func change_direction():
	var prev_direction = direction
	var player_pos = get_tree().get_current_scene().get_node("Player").global_position
	direction = -1 if (player_pos.x <= global_position.x) else 1
	if (direction != prev_direction):
		scale.x *= -1
		print(name + ": changing direction")

func reduce_cooldown(delta):
	if (cooldown_left > 0):
		cooldown_left -= delta

func spawn_raven():
	ravens_num += 1
	cooldown_left = RAVEN_SPAWN_COOLDOWN
	var raven = load("res://src/Enemies/RavenMaster/Raven.tscn").instance()
	var offset = RAVEN_SPAWN_RELATIVE_POSITION * Vector2(direction, 1)
	raven.creator = self
	raven.position = position + offset
	if (direction == 1):
		raven.scale.x *= -1
	get_parent().add_child(raven)
	print(name + ": spawning raven")
	anim_to_display = "SpawnRaven"

func raven_died():
	ravens_num -= 1

func die() -> void:
	if(anim_death == false):
		anim_to_display = "Death"
		death_lock = true
		return
	if (get_tree() != null):
		get_tree().get_current_scene().get_node("Player").get_point()
	queue_free()

func _on_DamageArea_area_entered(area: Area2D) -> void:
	if (area.is_in_group("Attack")):
		take_damage(area.get_parent().ATTACK_DAMAGE)
		
func print_type():
	return "RavenMaster"

func take_damage(damage: int) -> void:
	get_hit()
	dmg_anim = true
	print(name + " takes damage: " + String(damage))
	current_hp -= damage
	if current_hp <= 0:
		$DeathSound.play()
		die()

func get_hit():
	$GetHitSound.play()
