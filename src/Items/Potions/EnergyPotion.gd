extends Item
class_name EnergyPotion

export var MP_RESTORE_VALUE = 35

func take_effect(player):
	player.drink_potion()
	player.current_mp = min(player.current_mp + MP_RESTORE_VALUE, player.MP)
	print("Player: restored " + String(MP_RESTORE_VALUE) + " mp")
	self.queue_free()
