extends Node2D

#Afterimage store char of player from past frames and display it with alpha 

var type_mem:Array
var frame_mem:Array
var cords_mem:Array
var display_nr
var cur_size
var iter

func set_after_image(step_nr):
	display_nr = step_nr
	iter = 0
	for i in range(0,step_nr):
		cords_mem += [Vector2(-1,-1)]
		type_mem += [""]
		frame_mem += [-1]
	iter = 0
	

func save_after_image(cords:Vector2, type:String, frame:int):
	cords_mem[iter] = cords
	type_mem[iter] = type
	frame_mem[iter] = frame
	iter = (iter + 1) % display_nr
	
func show_anim():
	var offset_x = 0
	var offset_y = 2
	var alpha = 0.8
	var id_to_show = iter-1
	if(id_to_show < 0):
		id_to_show = display_nr - 1
	var through_all = false
	while(through_all == false):
		if(type_mem[id_to_show] != ""):
			offset_x -= cords_mem[id_to_show].x/40
			offset_y -= cords_mem[id_to_show].y/40
			get_children()[id_to_show].offset.x = offset_x
			get_children()[id_to_show].offset.y = offset_y
			get_children()[id_to_show].modulate.a = alpha
			alpha = alpha/2 
			get_children()[id_to_show].play(type_mem[id_to_show])
			get_children()[id_to_show].frame = frame_mem[id_to_show]
			get_children()[id_to_show].visible = true
		if(id_to_show == iter):
			through_all = true
		id_to_show = (id_to_show - 1)
		if(id_to_show == -1):
			id_to_show = display_nr-1 
		
func clear_anim():
	var id_to_show = iter-1
	if(id_to_show < 0):
		id_to_show = display_nr - 1
	var through_all = false
	while(through_all == false):
		if(type_mem[id_to_show] != ""):
			cords_mem += [Vector2(-1,-1)]
			type_mem += [""]
			frame_mem += [-1]
			get_children()[id_to_show].visible = false
		if(id_to_show == iter):
			through_all = true
		id_to_show = (id_to_show - 1)
		if(id_to_show == -1):
			id_to_show = display_nr - 1
			
	
			
		
