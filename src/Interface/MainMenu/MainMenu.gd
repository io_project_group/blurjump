extends Control

onready var selector_one = $CenterContainer/VBoxContainer/CenterContainer2/VBoxContainer/CenterContainer/HBoxContainer/Selector
onready var selector_two = $CenterContainer/VBoxContainer/CenterContainer2/VBoxContainer/CenterContainer2/HBoxContainer/Selector
onready var selector_three = $CenterContainer/VBoxContainer/CenterContainer2/VBoxContainer/CenterContainer3/HBoxContainer/Selector
var current_selection

func set_current_selection(choice):
	selector_one.text = ""
	selector_two.text = ""
	selector_three.text = ""
	current_selection = choice
	if choice == 0:
		selector_one.text = ">"
	elif choice == 1:
		selector_two.text = ">"
	elif choice == 2:
		selector_three.text = ">"

func _ready() -> void:
	set_current_selection(0)

func _process(delta):
	if Input.is_action_just_pressed("ui_down") and current_selection < 2:
		set_current_selection(current_selection + 1)
	elif Input.is_action_just_pressed("ui_up") and current_selection > 0:
		set_current_selection(current_selection - 1)
	elif Input.is_action_just_pressed("ui_accept"):
		if current_selection == 0:
			print("start game")
			get_tree().change_scene("res://src/Levels/Lvl1Waterfall/Lvl1Waterfall.tscn")
		elif current_selection == 1:
			print("tutorial")
			get_tree().change_scene("res://src/Levels/Tutorial/Tutorial.tscn")
		elif current_selection == 2:
			print("exit")
			get_tree().quit()
	
