extends Camera2D

export var LOOK_AHEAD_FACTOR = 0.1618
export var SHIFT_DURATION = 0.8
const SHIFT_TRANS = Tween.TRANS_SINE
const SHIFT_EASE = Tween.EASE_IN_OUT

var facing = 0

onready var prev_camera_pos = get_camera_position()
onready var tween = get_node("ShiftTween")

func _process(_delta: float) -> void:
	_check_facing()
	prev_camera_pos = get_camera_position()
	
func _check_facing() -> void:
	var new_facing = sign(get_camera_position().x - prev_camera_pos.x)
	if new_facing != 0 && facing != new_facing:
		facing = new_facing
		var target_offset = get_viewport_rect().size.x * LOOK_AHEAD_FACTOR * facing
		tween.interpolate_property(self, "position:x", position.x, target_offset, 
		SHIFT_DURATION, SHIFT_TRANS, SHIFT_EASE)
		tween.start()
	

func _on_Player_grounded_updated(onFloor) -> void:
	drag_margin_v_enabled = !onFloor
