extends Item
class_name FreezeToken

func take_effect(player: Player):
	player.take_token()
	if player.current_freeze_tokens < player.MAX_FREEZE_TOKENS:
		player.current_freeze_tokens += 1
		self.queue_free()
	print("Current freeze tokens: ", player.current_freeze_tokens)
