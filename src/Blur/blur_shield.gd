extends "res://src/Blur/blur.gd"

# Here should be all things special about shield
func _ready():
	if(is_moving_right):
		$BlurSprite.offset.x = -9
		$BlurSprite.offset.y = 2
		$BlurSprite.play("SpawnR")
	else:
		$BlurSprite.offset.x = 9
		$BlurSprite.offset.y = 2
		$BlurSprite.play("SpawnL")

func _physics_process(delta: float) -> void:
	handle_anim()
	
func handle_anim():
	if(life_length > 0.12):
		if(is_moving_right):
			$BlurSprite.offset.x = 0
			$BlurSprite.offset.y = 0
			$BlurSprite.play("BlurMoveR")
		else:
			$BlurSprite.offset.x = 0
			$BlurSprite.offset.y = 0
			$BlurSprite.play("BlurMoveL")
	if(life_length > MAX_LIFE_LENGTH/2):
		var tLin = life_length - MAX_LIFE_LENGTH/2
		$BlurSprite.modulate.a = 1-2*tLin/MAX_LIFE_LENGTH


func _on_PushAwayArea_entered(body: Node) -> void:
	if (body.get_class() == "Raven"):
		body.push_away()
