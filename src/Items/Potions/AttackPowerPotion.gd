extends Item
class_name AttackPowerPotion

export var ATTACK_BOOST_FACTOR = 2
export var BOOST_TIME = 5

var _player

func take_effect(player):
	player.drink_potion()
	_player = player
	player.ATTACK_DAMAGE *= ATTACK_BOOST_FACTOR
	var timer = $AttackPowerTimer
	timer.connect("timeout", self, "_on_AttackPowerTimer_timeout")
	timer.set_wait_time(BOOST_TIME)
	timer.start()
	$PickUpArea/PickUpShape.call_deferred("set_disabled", true)
	self.visible = false

func _on_AttackPowerTimer_timeout() -> void:
	_player.ATTACK_DAMAGE /= ATTACK_BOOST_FACTOR
	self.queue_free()
