extends Actor

#TODO Część zmiennych jest wspólna z Playerem -> można wrzucić do Actor
const NORMAL_VEC = Vector2(0,-1)
var GRAVITY := 500.0

var velocity := Vector2()
export var SPEED := 30.0
var is_turned_right

var triggered

export var SEARCHING_SPEED := 50.0
export var SEARCHING_BORDER := 30.0
var is_moving_down

export var CHANGING_COOLDOWN := 2.0
var changing_direction
var actual_changing_cooldown

export var BLOCK_LENGTH := 0.1
var PUSH_STRENGTH := 100.0
var push_away_mod
var block_movement
var block_timer
var will_die
var death_timer
var death_time = 0.3

var visible_timer
export var NOT_VISIBLE_LENGTH := 3.0

func _ready() -> void:
	block_movement = false
	push_away_mod = Vector2.ZERO
	visible_timer = 0.0
	will_die = false

func handle_searching_for_player(delta: float):
	if (is_moving_down):
		$Vision.rotation_degrees += SEARCHING_SPEED * delta
	else:
		$Vision.rotation_degrees -= SEARCHING_SPEED * delta
	
	if (abs($Vision.rotation_degrees) >= SEARCHING_BORDER):
		is_moving_down = !is_moving_down

func handle_normal_movement(delta: float):
	if (is_on_wall()):
		change_direction()
		
	if (triggered):
		handle_special_movement()
	else:
		if (changing_direction):
			handle_searching_for_player(delta)
			actual_changing_cooldown -= delta
			if (actual_changing_cooldown <= 0):
				change_direction()
	
	handle_move(delta)

func handle_special_movement():
	pass

func change_block_flags(delta):
	if(block_timer <= 0):
		block_movement = false
	else:
		block_timer -= delta

func handle_move(delta: float):
	var current_velocity
	if (block_movement):
		change_block_flags(delta)
		current_velocity = handle_push(velocity)
	else:
		current_velocity = calculate_velocity(delta)
		
	velocity = move_and_slide(current_velocity, NORMAL_VEC)

func handle_push(move_vec):
	if(push_away_mod.x != 0 or push_away_mod.y != 0):
		move_vec += push_away_mod
		if(push_away_mod.x * velocity.x < 0):
			velocity.x = 0
			move_vec.x = push_away_mod.x
		if(push_away_mod.y * velocity.y < 0):
			velocity.y = 0
			move_vec.y = push_away_mod.y
		push_away_mod = Vector2(0,0)
	return move_vec

func calculate_velocity(delta: float) -> Vector2:
	var current_velocity = velocity
	if is_on_floor() and !triggered and !changing_direction:
		if (is_turned_right):
			current_velocity.x = SPEED
		else:
			current_velocity.x = -SPEED
	current_velocity.y += GRAVITY * delta
	return current_velocity

func change_direction():
	$Vision.rotation_degrees = 0
	is_turned_right = !is_turned_right
	scale.x *= -1
	changing_direction = false

func push_away(player_position: Vector2):
	block_movement = true
	block_timer = BLOCK_LENGTH
	var push_direction = (self.position - player_position).normalized()
	push_away_mod = push_direction * PUSH_STRENGTH

func die():
	if(will_die == true):
		if (get_tree().get_current_scene() != null):
			get_tree().get_current_scene().get_node("Player").get_point()
		queue_free()
	else:
		will_die = true
		death_timer = 0

func check_if_on_screen(delta: float) -> bool:
	if ($VisibilityNotifier2D.is_on_screen()):
		visible_timer = NOT_VISIBLE_LENGTH
	else:
		visible_timer = min(0, visible_timer - delta)
	return visible_timer != 0

func check_if_fall_out():
	if (position.y > 1000):
		die()

func set_changing_direction(looking_down_first: bool):
	changing_direction = true
	velocity.x = 0
	actual_changing_cooldown = CHANGING_COOLDOWN
	is_moving_down = looking_down_first

func _on_GroundChecker_body_exited(body: Node) -> void:
	if (body.get_class() == "TileMap" and !changing_direction):
		set_changing_direction(true)

func _on_GroundChecker_area_entered(area: Area2D) -> void:
	if (area.is_in_group("Trap") and !changing_direction):
		set_changing_direction(false)

func _on_AreaChecker_area_entered(area: Area2D) -> void:
	if area.is_in_group("Attack"):
		take_damage(area.get_parent().ATTACK_DAMAGE)
		push_away(area.get_parent().get_position())
