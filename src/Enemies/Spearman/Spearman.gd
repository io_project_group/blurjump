extends "res://src/Enemies/WalkingEnemy/WalkingEnemy.gd"

export var CHARGING_DMG := 50.0
export var STABBING_DMG := 25.0

export var SPEED_MODIFIER := 3.0

var stabing_time = 0.6

var is_charging
var is_stabbing
var was_charging
var is_taking_dmg
var anim_timer
var dmg_take_time = 0.3
var enemy_id

func print_type():
	return "SpearMan"
func _ready() -> void:
	is_turned_right = true
	changing_direction = false
	triggered = false
	was_charging = false
	SEARCHING_SPEED = 0.0
	SEARCHING_BORDER = 0.0
	anim_timer = 0.0
	is_taking_damage = false

func _physics_process(delta: float) -> void:
	
	if(will_die == false):
		basic_rutine(delta)
		handle_anim(delta)
	else:
		death_anim(delta)

func basic_rutine(delta):
	check_if_fall_out()
	if check_if_on_screen(delta):
		handle_normal_movement(delta)

func handle_anim(delta):
	anim_timer += delta
	
	if(is_stabbing == true and anim_timer > stabing_time):
		is_stabbing = false
		$AnimatedSprite.offset.y = 0
		$AnimatedSprite.play("Walk")
	
	if(is_taking_damage == true):
		if(anim_timer > dmg_take_time):
			$AnimatedSprite.play("Walk")
			is_taking_damage = false
	
	if(is_charging == true):
		was_charging = true
	if(is_charging == false and was_charging == true):
		was_charging = false
		$AnimatedSprite.play("Walk")
		
	if(dmg_anim == true):
		$AnimatedSprite.play("DMG")
		dmg_anim = false
		anim_timer = 0
		is_taking_damage = true

func death_anim(delta):
	death_timer += delta
	if(death_timer > death_time):
		die()
	$AnimatedSprite.modulate.g = 1-2*death_timer/death_time
	$AnimatedSprite.modulate.b = 1-2*death_timer/death_time

func _on_Vision_body_entered(body: Node) -> void:
	if (body.get_class() == "Player"):
		$ScreamSound.play()
		print("KRZYK")
		SPEED *= SPEED_MODIFIER
		$SpearHorizontal/CollisionShape2D.disabled = false
		$AnimatedSprite.play("Charge")
		is_charging = true

func _on_Vision_body_exited(body: Node) -> void:
	if (body.get_class() == "Player"):
		SPEED /= SPEED_MODIFIER
		$SpearHorizontal/CollisionShape2D.disabled = true
		is_charging = false

func _on_SpearHorizontal_body_entered(body: Node) -> void:
	if (body.get_class() == "Player"):
		body.take_damage(CHARGING_DMG)
		body.push_away(self.position, velocity)


func _on_SpearVertically_body_entered(body: Node) -> void:
	if (body.get_class() == "Player"):
		anim_timer = 0
		$AnimatedSprite.play("AirAttack")
		$AnimatedSprite.offset.y = -15
		is_stabbing = true
		body.take_damage(STABBING_DMG)
		body.push_away(self.position, velocity)

func take_damage(damage: int) -> void:
	get_hit()
	dmg_anim = true
	print(name + " takes damage: " + String(damage))
	current_hp -= damage
	if current_hp <= 0:
		$DeathSound.play()
		die()

func get_hit():
	$GetHitSound.play()
