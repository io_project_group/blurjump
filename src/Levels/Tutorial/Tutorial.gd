extends Node2D

func _ready() -> void:
	$RavenMaster.set_physics_process(false)

func _on_RavenActivationArea_body_entered(body: Node) -> void:
	if body.get_class() == 'Player':
		if get_tree().get_current_scene().has_node("RavenMaster"):
			$RavenMaster.set_physics_process(true)

func _on_Win_body_entered(body: Node) -> void:
	if body.get_class() == 'Player':
		print("Tutorial end. You won!")
		get_tree().change_scene("res://src/Interface/MainMenu/MainMenu.tscn")
