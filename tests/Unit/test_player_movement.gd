extends "res://addons/gut/test.gd"

var character = load("res://src/Player/Player.gd")

var _character

func before_each():
	_character = character.new()

func after_each():
	_character.free()
	
# In this test we set character state to on flor, force jump and
# Check if variables that store time to specyfic accions have valid
# at the moment of jump and in next call - "simulated" frame after

func test_handle_jump():
	_character.set_physics_process(false)
	_character._ready()
	#set state on floor in player
	_character.on_floor = true
	
	_character.handle_jump(0.01, true)

	#check if jump was performed
	assert_true(_character.player_velocity.y < 0 , "Player can't jump")
	
	#check if fall handicap was set to safe value
	var fall_handicap = _character.FALL_HANDICAP
	assert_true(_character.fall_time > fall_handicap, 
			"Fall handicap count wasn't deleted")
	
	#check if jump time was set to 0
	assert_true(_character.jump_time == 0, 
			"Jump time was not reset")
	_character.on_floor = false
	_character.handle_jump(0.01, true)
	
	#check if jump time was incremented by time step
	assert_true(_character.jump_time == 0.01, 
			"Jump time was not incremented")
	
func test_handle_move():
	_character.set_physics_process(false)
	_character._ready()
	#set state on floor in player
	_character.on_floor = true
	
	_character.handle_move(0.01, 1)
	
	#check if move was performed
	assert_true(_character.player_velocity.x > 0 , "Player can't move")
	
	#check if static friction timer was reset
	assert_true(_character.static_time == 0, 
			"Static friction timer was not reset")
	
	_character.handle_move(0.01, 1)
	
	#check if static friction timer was increment
	assert_true(_character.static_time == 0.01, 
			"Static friction timer was not incremented")
			

	  
func test_handle_attack():
	var loaded_scene = load("res://src/Player/Player.tscn")
	var player_scene = loaded_scene.instance()
	var character = player_scene.get_node(".")
	character.set_physics_process(false)
	character._ready()
	#set state on floor in player
	character.on_floor = true
	
	assert_true(character.combatAttackPoints == 3, "combatAttackPoints was not declered properly")
	
	character.handle_attack(3) 
	assert_true(character.combatAttackPoints == 2, "combatAttackPoints was not changed")
	character.handle_attack(2)
	assert_true(character.combatAttackPoints == 1, "combatAttackPoints was not changed")
	character.handle_attack(1)
	assert_true(character.combatAttackPoints == 0, "combatAttackPoints was not changed")
	player_scene.free()
