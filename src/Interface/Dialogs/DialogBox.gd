extends Control

enum {READY, STARTED}

export var dialog = [
	'Placeholder text\nWelcome to Blur Jump !!! \naaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
	'Other text',
	'And the last one !!!'
]
export var deactivation_dist = 100

onready var dialog_index = 0
onready var starting_pos = get_tree().get_current_scene().get_node('Player').position

func _ready() -> void:
	load_next_dialog()

func _process(delta: float) -> void:
	var player_pos = get_tree().get_current_scene().get_node('Player').position
	if starting_pos.distance_to(player_pos) > deactivation_dist:
		queue_free()
	if Input.is_action_just_pressed('ui_accept'):
		load_next_dialog()

func load_next_dialog():
	if dialog_index < dialog.size():
		$Background/Text.bbcode_text = dialog[dialog_index]
		dialog_index += 1
	else:
		queue_free()
