extends "res://addons/gut/test.gd"

var parent
var player
var hud

func before_each():
	player = Player.new()
	parent = Node.new()
	parent.add_child(player)
	parent.get_child(0).name = "Player"
	player._ready()
	hud = preload("res://src/Hud/Hud.tscn").instance()
	parent.add_child(hud)
	hud._ready()
	
func after_each():
	parent.free()
	
func test_hud_hp_init():
	assert_true(hud.get_node("Health").get_node("HP").scale.x == -86,
			"failed: hud_hp_init")
			
func test_hud_hp():
	var dmg = player.HP/2;
	player.take_damage(dmg)

	hud._physics_process(0.1)
	assert_true(hud.get_node("Health").get_node("HP").scale.x == -43,
		"failed: hud_hp")

func test_mp_init():
	assert_true(hud.get_node("Mana").get_node("MP").scale.x == -86,
		"failed: hud_mp_init")
		
func test_mp():
	player.current_mp = player.MP/2
	hud._physics_process(0.1)
	assert_true(hud.get_node("Mana").get_node("MP").scale.x == -43,
		"failed: hud_mp")
		
func test_token_init():
	var counter = 0
	var token_iter = "Token"
	var token_name
	for i in range (1,7):
		token_name = token_iter + str(i)
		if(hud.get_node("Tokens").get_node(token_name).visible == true):
			counter += 1
	assert_true(counter == player.START_FREEZE_TOKENS,
			"failed: hud_token_init")
			
func test_token():
	player.current_freeze_tokens = 5
	hud._physics_process(0.1)
	var counter = 0
	var token_iter = "Token"
	var token_name
	for i in range (1,7):
		token_name = token_iter + str(i)
		if(hud.get_node("Tokens").get_node(token_name).visible == true):
			counter += 1
	assert_true(counter == 5,
			"failed: hud_token_init")
