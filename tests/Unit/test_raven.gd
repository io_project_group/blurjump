extends "res://addons/gut/test.gd"

var test_level_scene = load("res://src/Levels/TestLevel.tscn")
var raven_scene = load("res://src/Enemies/RavenMaster/Raven.tscn")

var test_level
var raven

func before_each():
	test_level = test_level_scene.instance()
	raven = raven_scene.instance()
	test_level.add_child(raven)
	raven._ready()

func after_each():
	test_level.free()

func test_change_to_attacking():
	raven.player.position.x = raven.position.x + raven.ATTACK_RANGE - 1
	raven.handle_flying()
	assert_true(raven.state == raven.ATTACKING)

func test_change_to_turning_after_attacking():
	raven.state = raven.ATTACKING
	raven.global_position = Vector2(1, 0)
	raven.handle_attacking()
	assert_true(raven.state == raven.TURNING)

func test_change_to_attacking_after_turning():
	raven.state = raven.TURNING
	raven.global_position = Vector2(1, 0)
	raven.handle_turning()
	assert_true(raven.state == raven.ATTACKING)

func test_dealing_damage():
	var player = raven.player
	player.current_hp = player.HP
	raven._on_AttackArea_entered(player)
	assert_true(player.current_hp < player.HP)

func test_died():
	raven.current_hp = 10
	raven.take_damage(100)
	assert_true(raven.current_hp < 0)
