extends "res://src/Enemies/WalkingEnemy/WalkingEnemy.gd"

export var SHOOTING_COOLDOWN := 2.0
export var DMG := 25

var attack_time
var target
var anim_timer
var is_shoting
var is_taking_dmg
var shot_anim_time = 0.7
var shot_anim_frac = 0.5
var dmg_take_time = 0.3
var enemy_id

var bullet_resource

func _ready() -> void:
	is_turned_right = true
	changing_direction = false
	triggered = false
	attack_time = 0.0
	bullet_resource = preload("res://src/Enemies/Musketeer/Bullet.tscn")
	anim_timer = 0
	is_shoting = false
	is_taking_damage = false

func _physics_process(delta: float) -> void:
	#$GetHitSound.stop()
	if(will_die == false):
		basic_rutine(delta)
		anim_handling(delta)
	else:
		death_anim(delta)

func death_anim(delta):
	death_timer += delta
	if(death_timer > death_time):
		die()
	$AnimatedSprite.modulate.g = 1-2*death_timer/death_time
	$AnimatedSprite.modulate.b = 1-2*death_timer/death_time

func basic_rutine(delta):
	check_if_fall_out()
	if check_if_on_screen(delta):
		attack_time = max(0, attack_time - delta)
		if (triggered):
			attack_if_possible(delta)
		handle_normal_movement(delta)

func anim_handling(delta):
	anim_timer += delta
	if(is_shoting == true):
		if(anim_timer > shot_anim_time):
			anim_timer = 0
			$AnimatedSprite.play("Walk")
			is_shoting = false
	
	if(is_taking_damage == true):
		if(anim_timer > dmg_take_time):
			$AnimatedSprite.play("Walk")
			is_taking_damage = false
	
	if(dmg_anim == true):
		anim_timer = 0
		$AnimatedSprite.play("DMG")
		dmg_anim = false
		is_taking_damage = true

func attack_if_possible(delta: float):
	if(attack_time <= 0 and is_shoting == false):
		anim_timer = 0
		is_shoting = true
		$AnimatedSprite.play("Shot") 
	if (attack_time <= 0 and anim_timer > 0.5 * shot_anim_frac):
		shoot_bullet()
		attack_time = SHOOTING_COOLDOWN

func shoot_bullet():
	$MusketShotSound.play()
	var space_state = get_world_2d().direct_space_state
	var result = space_state.intersect_ray(position, target.position, [self])
	if (result and result.collider.name == "Player"):
		var bullet = bullet_resource.instance()
		bullet.set_target_and_damage(target.position, self.position, DMG)
		owner.add_child(bullet)

func _on_Vision_body_entered(body: Node) -> void:
	if (body.get_class() == "Player"):
		triggered = true
		target = body

func _on_Vision_body_exited(body: Node) -> void:
	if (body.get_class() == "Player"):
		triggered = false

func print_type():
	return "Muscater"

func take_damage(damage: int) -> void:
	get_hit()
	dmg_anim = true
	print(name + " takes damage: " + String(damage))
	current_hp -= damage
	if current_hp <= 0:
		$DeathSound.play()
		die()

func get_hit():
	$GetHitSound.play()
