extends "res://addons/gut/test.gd"

var freeze := load("res://src/WorldState/Freeze.gd")

var _freeze = null

func before_each():
	_freeze = freeze.new()

func after_each():
	_freeze.free()
	
func test_can_create_Freeze():
	assert_not_null(_freeze)
	
func test_get_eps():
	var temp = _freeze.get_eps()
	assert_eq(temp, 5)

func test_init_freeze():
	var temp = _freeze.get_freeze()
	assert_false(temp)
	
func test_freeze_time():
	_freeze.freeze_active = true
	_freeze.time_start = OS.get_ticks_msec() + 10;
	_freeze.duration_ms = _freeze.DURATION * 1000
	_freeze.freeze_time()
	assert_true(_freeze.freeze_active)
	
func test_freeze_time_false():
	_freeze.freeze_active = true
	_freeze.time_start = -1 * _freeze.DURATION * 1000;
	_freeze.duration_ms = _freeze.DURATION * 1000
	_freeze.freeze_time()
	assert_false(_freeze.freeze_active)

