extends Actor

class_name Player

#Parameters for freeze tokens
export var MAX_FREEZE_TOKENS := 5
export var START_FREEZE_TOKENS := 3
var current_freeze_tokens

signal grounded_updated(is_grounded)

# player_velocity is speed of player character,
# NORMAL_VAC - vector normal to surface (for is_on_floor)
var player_velocity:Vector2
var push_away_mod:Vector2
const NORMAL_VEC = Vector2(0,-1)

# Here are constant parameters for jump physics
export var GRAVITY_UP = 1000
export var GRAVITY_DOWN = 2000

# set
export var GRAVITY_FRAC = 0.5

# Max vertical speed is determined by viscosity q via: a = g +/- qv^2
export var VISCOSITY = 0.01
export var JUMP_SPEED = -300

# Parameters for horizontal movement max horizontal speed
export var SPEED_MAX = 150
export var ACC = 500
export var FRICTION = 1000
export var AIR_FRIC_FRAC = 0.8

# Constant to simulate resistance at begining of move 
export var STATIC_FRIC_TIME = 0.1
export var STATIC_FRIC_FRAC = 0.05

# Parameters that de termine HANDICAP for player (key time tolerance, 
# time to jump after fall, dependent on key_time jump high) 
export var FALL_HANDICAP = 0.05
export var JUMP_KEY_HANDICAP = 0.25

#Constant to modulate jump high
export var JUMP_MODULATION_TRESHOLD = 2
var JMP_STATES = [-140, -60, 60, 200, 300]

# Here are variables that are used to count time for HANDICAPs, acceleration etc.
var static_time
var fall_time
var key_time
var jump_time
var jump_key_window

# Variables that describe player status
var on_floor
var was_on_floor
var last_move_right
var direction
var animation_lock
var animation_time
var animation_type
var animation_should_unlock
var was_frozen
var animation_frame
var animation_afterimg_timer
var test_state
export var push_force = 100
var block_movement
var block_timer

# Preloaded resources
var blur_shield_resource
var blur_platform_resource
 
# freeze state 

var freeze_stuff
var freeze_state
var is_freeze_active
signal freeze_start()
var first_iteration = true

var CHARGE_ATTACK_LOCK = false
const CHARGE_ATTACK_MP = 55
var combatAttackPoints = 3
var is_attacking 
var attacking_anim
const ATTACK1_FRAME = 6
const ATTACK2_FRAME = 8
const ATTACK1_RAND_RANGE = 4
const ATTACK_RAND_RANGE = 5
export var ATTACK_DAMAGE = 50
var attack_anims = ["Attack1R", "Attack1L", "Attack2R", "Attack2L" ,"Attack3R", "Attack3L"]

var is_on_blur
var blur_platform
const BLUR_SHIELD_COST = 30
const BLUR_PLATFORM_COST = 30

var current_score
var current_attack_damage
var walking_water_playing = false
var walking_ground_playing = false
var stay_in_place = true
var will_die
var death_timer
var death_time = 0.5
func _ready():
	fall_time = 0
	key_time = JUMP_KEY_HANDICAP + 0.1
	jump_time = 0
	static_time = 0
	on_floor = false 
	direction = true
	jump_key_window = false
	last_move_right = true
	blur_shield_resource = preload("res://src/Blur/BlurShield.tscn")
	blur_platform_resource = preload("res://src/Blur/BlurPlatform.tscn")
	animation_lock = false
	animation_time = 0
	animation_should_unlock = false
	#for testing purpose we guard current_scene = null
	if(get_tree() != null):
		freeze_state = get_parent().get_node("WorldState").get_node("Freeze")
		is_freeze_active = freeze_state.get_freeze()
	was_frozen = false
	animation_afterimg_timer = 0
	push_away_mod.x = 0
	push_away_mod.y = 0
	block_movement = false
	block_timer = 0
	current_hp = HP
	current_mp = MP
	current_score = 0
	is_attacking = false
	is_on_blur = false
	current_freeze_tokens = START_FREEZE_TOKENS
	current_attack_damage = ATTACK_DAMAGE


func _process(_delta: float) -> void:
	if !(get_node("VisibilityNotifier").is_on_screen()):
		die()

func _physics_process(delta):
	if !will_die:
		#check world states
		freeze_time(false)
		
		was_on_floor = on_floor
		check_animation_lock()
		
		if(block_movement):
			change_block_flags(delta)
				
		if(!block_movement):
			handle_jump(delta, false)
			handle_move(delta, 0)
		
		handle_animation(delta)
		
		if(player_velocity.y == 0):
			player_velocity.y = 1 
			
		var move_vec = handle_push(player_velocity)

		if (is_on_blur and is_on_floor()):
			self.position += blur_platform.blur_speed * delta
		
		if(CHARGE_ATTACK_LOCK):
			var temp:Vector2
			temp.x = move_vec.x
			player_velocity = move_and_slide(temp, NORMAL_VEC)
		else:
			player_velocity = move_and_slide(move_vec, NORMAL_VEC)
			
		on_floor = is_on_floor()
		handleBlurShield(false)
		handleBlurPlatform()
		handle_camera()
		handle_attack(0)
	else:
		death_anim(delta)

func death_anim(delta):
	death_timer += delta
	if(death_timer > death_time):
		die()
	$HeroSprite.modulate.g = 1-2*death_timer/death_time
	$HeroSprite.modulate.b = 1-2*death_timer/death_time
	
func die():
	if(will_die == true):
		var tree = get_tree()
		if tree:
			tree.reload_current_scene()
	else:
		will_die = true
		death_timer = 0
		
func change_block_flags(delta):
	if(block_timer <= 0):
		block_movement = false
	else:
		block_timer -= delta
		
func handle_push(move_vec):
	if(push_away_mod.x != 0 or push_away_mod.y != 0):
		move_vec += push_away_mod
		if(push_away_mod.x * player_velocity.x < 0):
			player_velocity.x = 0
			move_vec.x = push_away_mod.x
		if(push_away_mod.y * player_velocity.y < 0):
			player_velocity.y = 0
			move_vec.y = push_away_mod.y
		push_away_mod = Vector2(0,0)
	return move_vec
	
#function check if control should be taken from player to perform attack animation
func check_animation_lock():
	if(!animation_lock):
		if(Input.is_action_just_pressed("make_blur_shield") and !is_attacking):
			create_blur_sound()
			animation_lock = true
			animation_type = "shield"
		if(Input.is_action_just_pressed("make_blur_platform") and !is_attacking):
			create_blur_sound()
			animation_lock = true
			animation_type = "platform"

#force can be used to force up key input
func handle_jump(delta, force):
	fall_time += delta
	key_time += delta
	jump_time += delta
	
	# gravTemp let modifying Gravity dependent on logic in frame
	var grav_temp = GRAVITY_UP
	
	# accY is acceleration during curent frame along oy
	var acc_y = 0
	
	# viscoSign check if player is falling or rising for vertical acceleration calc
	var visco_sign = 1
	if (player_velocity.y > 0):
		visco_sign = -1
		grav_temp = GRAVITY_DOWN

	var viscosity_correction =  player_velocity.y * player_velocity.y * VISCOSITY
	acc_y = max(0, (grav_temp + visco_sign * viscosity_correction))
	
	if (jump_time < JUMP_MODULATION_TRESHOLD and Input.is_action_pressed("jump") and !is_attacking):
		acc_y = acc_y * GRAVITY_FRAC
	
	if (on_floor):
		player_velocity.y = 0 
		fall_time = 0
	
	if(Input.is_action_just_pressed("jump") and !is_attacking):
		key_time = 0
	
	if (on_floor or fall_time < FALL_HANDICAP):
		if(Input.is_action_just_pressed("jump") and !is_attacking 
		or key_time < JUMP_KEY_HANDICAP or force):
			jump_sound()
			jump_time = 0
			fall_time = FALL_HANDICAP + 0.1
			player_velocity.y = JUMP_SPEED
			
	if(fall_time < FALL_HANDICAP):
		acc_y = 0
		
	player_velocity.y += acc_y*delta

func move_player(delta, dir_value, static_fric_mod):
	# accX is acceleration during current frame along oX
	var acc_x = static_fric_mod * ACC * dir_value
	if (player_velocity.x == 0):
		static_time = 0
	if (get_node("AttackingArea")):
		$AttackingArea.set_scale(Vector2(dir_value, dir_value))
		$ChargeAttack.set_scale(Vector2(dir_value, dir_value))
	player_velocity.x += acc_x * delta
	direction = (dir_value == 1)
	if (dir_value == 1):
		player_velocity.x = min(player_velocity.x, SPEED_MAX)
	else:
		player_velocity.x = max(player_velocity.x, -SPEED_MAX)
	return acc_x

func handle_move(delta, force):
	var move_pressed = false
	var static_fric_mod = 1
	
	static_time += delta
	if(static_time < STATIC_FRIC_TIME and on_floor):
		static_fric_mod = STATIC_FRIC_FRAC
		
	if ((Input.is_action_pressed("move_right") and 
			!Input.is_action_pressed("move_left") and !is_attacking) or force == 1):
			move_pressed = true
			stay_in_place = false
			move_player(delta, 1, static_fric_mod)
	elif ((Input.is_action_pressed("move_left") and 
			!Input.is_action_pressed("move_right") and !is_attacking) or force == -1):
			move_pressed = true
			stay_in_place = false
			move_player(delta, -1, static_fric_mod)
	if (!move_pressed):
		stay_in_place = true
		var friction_calc = FRICTION
		if (!on_floor):
			friction_calc *= AIR_FRIC_FRAC
		if (player_velocity.x > 0):
			player_velocity.x -= delta * friction_calc
			player_velocity.x = max(0, player_velocity.x)
		elif (player_velocity.x < 0):
			player_velocity.x += delta * friction_calc
			player_velocity.x = min(0, player_velocity.x)

func handle_animation(delta):
	if(animation_should_unlock):
		animation_lock = false
		animation_should_unlock = false
		$HeroSprite.offset.y = 0
	
	if(!animation_lock):
		anim_divisible(delta)
	else:
		anim_indivisible(delta)
		
	anim_scripted(delta)	
	animation_frame = $HeroSprite.frame

#handling of animation that don't need lock change
func anim_divisible(_delta):
	if(player_velocity.x == 0 and !is_attacking and on_floor): 
		anim_play_iddle()
	if(player_velocity.x != 0 and player_velocity.y == 0 and !is_attacking):
		anim_play_walk()
	if(player_velocity.y != 0 and !is_attacking and !is_on_wall()):
		anim_play_jump()

func anim_play_left_right(anim_left: String, anim_right: String):
	$HeroSprite.play(anim_right) if direction else $HeroSprite.play(anim_left)

func anim_play_iddle():
	anim_play_left_right("IddleL", "IddleR")

func handle_sound_playing():
	if(!stay_in_place and is_on_floor() and is_in_water):
		walk_water()
	elif(!stay_in_place and is_on_floor() and !is_in_water):
		walk_ground()
	elif(stay_in_place):
		turn_off_sounds()

func anim_play_walk():
	handle_sound_playing()
	anim_play_left_right("WalkL", "WalkR")

func anim_play_jump():
	turn_off_sounds()
	
	anim_play_left_right("JumpL", "JumpR")
	if(player_velocity.y < JMP_STATES[0]):
		$HeroSprite.frame = 1
	for i in range(0, 4):
		if(JMP_STATES[i] < player_velocity.y and player_velocity.y < JMP_STATES[i + 1]):
			$HeroSprite.frame = i + 2
	if(player_velocity.y > JMP_STATES[4]):
		$HeroSprite.frame = 6

func anim_indivisible(delta):
	animation_time += delta
	spawn_blur(delta, "ShieldR", "ShieldL", 0.4)
	spawn_blur(delta, "PlatformR", "PlatformL", 0.35)
		
func spawn_blur(delta, anim_right, anim_left, spawn_time):
	if(animation_time == delta):
		$HeroSprite.offset.y = -4
		anim_play_left_right(anim_left, anim_right)
	if(animation_time >= spawn_time):
		animation_should_unlock = true
		animation_time = 0

func anim_scripted(delta):
	anim_afterimage(delta)

func anim_afterimage(delta):
	if(is_freeze_active):
		if(!was_frozen):
			was_frozen = true
			$AfterImage.set_after_image(6)
			$AfterImage.save_after_image(player_velocity,$HeroSprite.animation,$HeroSprite.frame)
		if(animation_frame != $HeroSprite.frame or animation_afterimg_timer > 0.2):
			animation_afterimg_timer = 0
			$AfterImage.show_anim()
			$AfterImage.save_after_image(player_velocity,$HeroSprite.animation,$HeroSprite.frame)
		animation_afterimg_timer += delta
	else:
		if(was_frozen):
			was_frozen = false
			$AfterImage.clear_anim()

func handleBlurShield(force):
	if (Input.is_action_just_pressed("make_blur_shield") or force) and current_mp - BLUR_SHIELD_COST >= 0:
		current_mp -= BLUR_SHIELD_COST
		var blur = blur_shield_resource.instance()
		blur.set_to_player(position, direction)
		owner.add_child(blur)

func handleBlurPlatform():
	if Input.is_action_just_pressed("make_blur_platform") and current_mp - BLUR_PLATFORM_COST >= 0:
		current_mp -= BLUR_PLATFORM_COST
		var blur = blur_platform_resource.instance()
		blur.set_to_player(position, direction)
		owner.add_child(blur)

func handle_camera():
	if was_on_floor == null || was_on_floor != on_floor:
		emit_signal("grounded_updated", on_floor)	

func freeze_time(force: bool):
	is_freeze_active = freeze_state.get_freeze()

	if not is_freeze_active and current_freeze_tokens > 0:
		if Input.is_action_just_pressed("freeze") or force:
			emit_signal("freeze_start")
			current_freeze_tokens -= 1
			print("Current freeze tokens: ", current_freeze_tokens)




func push_away(point:Vector2, enem_vel:Vector2):
	var push_dir = Vector2(self.position.x-point.x, self.position.y-point.y) 
	var normalize = push_dir.x * push_dir.x + push_dir.y * push_dir.y
	normalize = sqrt(normalize)
	push_dir.x = push_dir.x / normalize
	push_dir.y = push_dir.y/normalize
	push_dir *= push_force
	push_away_mod = push_dir
	if(enem_vel.x * player_velocity.x < 0 or enem_vel.y * player_velocity.y < 0
			or (player_velocity.x == 0 and enem_vel.x != 0) or 
			(player_velocity.y == 0 and enem_vel.y != 0)):
		push_away_mod *= 2
	block_movement = true
	block_timer = 0.03
	
func take_damage(damage: int) -> void:
	get_hit()
	dmg_anim = true
	print(name + " takes damage: " + String(damage))
	current_hp -= damage
	if current_hp <= 0:
		death_sound()
		die()


func ground_attack(force):
	if (combatAttackPoints == 3 or force == 3):
		$CombatAttackTimer.start()
		anim_play_left_right("Attack1L", "Attack1R")
		$AttackingArea/Attack1.disabled = !($HeroSprite.frame < ATTACK1_FRAME)
		combatAttackPoints -= 1
		attack_sound()
	elif((combatAttackPoints == 2 and $HeroSprite.frame > 3) or force == 2):
		$CombatAttackTimer.start()
		anim_play_left_right("Attack2L", "Attack2R")
		$AttackingArea/Attack2.disabled = !($HeroSprite.frame < ATTACK1_FRAME)
		combatAttackPoints -= 1
		attack_sound()
		current_attack_damage += 5
	elif((combatAttackPoints == 1 and $HeroSprite.frame > 4 ) or force == 1):
		anim_play_left_right("Attack3L", "Attack3R")
		$CombatAttackTimer.start()
		$AttackingArea/Attack1.disabled = true
		$AttackingArea/Attack2.disabled = true
		$AttackingArea/Attack3.disabled = !($HeroSprite.frame < ATTACK2_FRAME)
		combatAttackPoints -= 1
		current_attack_damage += 7
		attack_sound()

func handle_attack(force):
	if(Input.is_action_just_pressed("Attack") or force != 0):
			turn_off_sounds()
			
			is_attacking = true
			ground_attack(force)
	if(Input.is_action_just_pressed("ChargeAttack")):
		if (is_on_floor()):
			#is_attacking = true
			$ChargeAttack/ChargeAttackTimer.start()
			CHARGE_ATTACK_LOCK = true;
			$ChargeAttack/ChargeAttackShape.disabled = false
			ACC = 1500
			SPEED_MAX = 100000

func finish_charge_attack():
	$ChargeAttack/ChargeAttackShape.disabled = true
	CHARGE_ATTACK_LOCK = false
	SPEED_MAX = 150
	ACC = 300
	is_attacking = false

func get_class():
	return "Player"

func get_point() -> void:
	current_score += 1
	print("Player got a point. Player has " + str(current_score) + " points.")


func _on_HeroSprite_animation_finished():
	if ($HeroSprite.animation in attack_anims):
		$AttackingArea/Attack1.disabled = true
		$AttackingArea/Attack2.disabled = true
		$AttackingArea/Attack3.disabled = true
		current_attack_damage = ATTACK_DAMAGE
		is_attacking = false

func _on_CombatAttackTimer_timeout():
	combatAttackPoints = 3
	current_attack_damage = ATTACK_DAMAGE

func _on_ChargeAttackTimer_timeout():
	finish_charge_attack()

func _on_ChargeAttack_area_entered(area):
	finish_charge_attack()

func _on_BlurDetection_body_entered(body: Node) -> void:
	if (body.is_in_group("BlurPlatform")):
		is_on_blur = true
		blur_platform = body

func _on_BlurDetection_body_exited(body: Node) -> void:
	if (body.is_in_group("BlurPlatform")):
		is_on_blur = false


func _on_HPTimer_timeout():
	current_hp = min(current_hp + 1, HP)


func _on_MPTimer_timeout():
	current_mp = min(current_mp + 1, MP)

func create_blur_sound():
	$CreatingBlurSound.play()

func jump_sound():
	if($HeroJumpSound != null):
		$HeroJumpSound.play()
func drink_potion():
	if($PotionSound != null):
		$PotionSound.play()
	
func take_token():
	$TokenSound.play()

func get_hit():
	if($GetHitSound != null):
		$GetHitSound.play()

func attack_sound():
	$HeroAttackSound.play()
func death_sound():
	$HeroDeathSound.play()
func walk_water():
	$WalkSound.stop()
	if(!walking_water_playing):
		walking_water_playing = true
		$WalkingWater.play()
	walking_ground_playing = false
	
func walk_ground():
	$WalkingWater.stop()
	if(!walking_ground_playing):
		walking_ground_playing = true
		$WalkSound.play()
	
	walking_water_playing = false


func turn_off_sounds():
	if(walking_water_playing):
		$WalkingWater.stop()
		walking_water_playing = false
	elif(walking_ground_playing):
		$WalkSound.stop()
		walking_ground_playing = false
