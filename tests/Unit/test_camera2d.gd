extends "res://addons/gut/test.gd"

var player_scene = load("res://src/Player/Player.tscn")

var _player_scene = null

func before_each():
	_player_scene = player_scene.instance()

func after_each():
	_player_scene.free()

# Checks if facing is being change properly when camera is moving.
func test_check_facing():
	var camera = _player_scene.get_node("Camera2D")
	camera.tween = _player_scene.get_node("Camera2D/ShiftTween")
	camera.prev_camera_pos = Vector2(-1000, 0)
	camera._check_facing()
	assert_true(camera.facing == 1)
	
	camera.prev_camera_pos = Vector2(1000, 0)
	camera._check_facing()
	assert_true(camera.facing == -1)
