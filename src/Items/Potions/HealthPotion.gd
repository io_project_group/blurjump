extends Item
class_name HealthPotion

export var HP_RESTORE_VALUE = 50

func take_effect(player):
	player.drink_potion()
	player.current_hp = min(player.current_hp + HP_RESTORE_VALUE, player.HP)
	print("Player: restored " + String(HP_RESTORE_VALUE) + " hp")
	self.queue_free()
	
