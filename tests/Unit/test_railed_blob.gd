extends "res://addons/gut/test.gd"

var loaded = load("res://src/Levels/TestLevel.tscn")
var loaded2 = load("res://src/Levels/TestLevel.tscn")

var loaded_lvl
var loaded_lvl2

func before_each():
	loaded_lvl = loaded.instance()
	loaded_lvl2 = loaded2.instance()

func after_each():
	loaded_lvl.free()
	loaded_lvl2.free()

# In this test we set character state to on flor, force jump and
# Check if variables that store time to specyfic accions have valid
# at the moment of jump and in next call - "simulated" frame after

func test_enemy_creation():
	var enem = loaded_lvl.get_node("EnemyMenager").get_child(0)
	enem._ready()
	loaded_lvl.get_node("EnemyMenager")._ready()
	
	#test if enemy id was asigned:
	assert_true(enem.enemy_id == 1,
			"Enemy 1 id not asigned")
	
func test_enemy_movement():
	var enem = loaded_lvl.get_node("EnemyMenager").get_child(0)
	var freeze_state = loaded_lvl.get_node("WorldState").get_node("Freeze")
	enem._ready()
	var simple_path = [Vector2(-1,0), Vector2(0,-1)]
	enem.set_path(simple_path)
	enem.freeze_state = freeze_state
	enem._physics_process(0.1)
	assert_true(enem.enemy_velocity.x < 0,
			"Enemy 1 cannot move")

func test_enemy_slow():
	var enem = loaded_lvl.get_node("EnemyMenager").get_child(0)
	var freeze_state = loaded_lvl.get_node("WorldState").get_node("Freeze")
	enem._ready()
	var simple_path = [Vector2(-1,0), Vector2(0,-1)]
	enem.set_path(simple_path)
	enem.freeze_state = freeze_state
	enem._physics_process(0.01)
	enem.freeze_frac = 0.5
	var vel = enem.cur_velocity
	print(vel)
	var norm1_vel = vel.x
	if(vel.x < 0):
		norm1_vel *= -1
	if(vel.y < 0):
		norm1_vel -= vel.y
	else:
		norm1_vel += vel.y
	freeze_state._freeze_start()
	enem._physics_process(0.01)
	vel = enem.cur_velocity
	print(vel)
	var norm1_vel2 = vel.x
	if(vel.x < 0):
		norm1_vel2 *= -1
	if(vel.y < 0):
		norm1_vel2 -= vel.y
	else:
		norm1_vel2 += vel.y
	
	assert_true( norm1_vel2 < norm1_vel,
			"Enemy wasn't slowed")

func test_enemy_attack():
	var player = loaded_lvl.get_node("Player")
	player._ready()
	var hp_before = player.current_hp
	var enemy = loaded_lvl.get_node("EnemyMenager").get_node("RailedBlob1")
	enemy._ready()
	enemy.get_node("AttackField").emit_signal("body_entered",player)
	var hp_after = player.current_hp
	assert_true( hp_before > hp_after,
			"Test enemy didn't hurt player")

func test_enemy_take_dmg():
	var enemy = loaded_lvl.get_node("EnemyMenager").get_node("RailedBlob1")
	enemy._ready()
	var hp_before = enemy.current_hp
	enemy.take_damage(2)
	var hp_after = enemy.current_hp
	assert_true( hp_before > hp_after,
			"Enemy can't be hurt")
			
func test_enemy_die():
	var enemy = loaded_lvl2.get_node("EnemyMenager").get_node("RailedBlob1")
	enemy._ready()
	var hp_before = enemy.current_hp
	enemy.take_damage(hp_before+1)
	var hp_after = enemy.current_hp
	assert_true( enemy.active == false,
			"Enemy wasn't assigned to die")
