extends "res://addons/gut/test.gd"

var test_level_scene = load("res://src/Levels/TestLevel.tscn")
var dialog_box_scene = load("res://src/Interface/Dialogs/DialogBox.tscn")
var activator_scene = load("res://src/Interface/Dialogs/DialogActivator.tscn")

var test_level
var player
var dialog_box
var activator

func before_each():
	test_level = test_level_scene.instance()
	player = test_level.get_node("Player")
	dialog_box = dialog_box_scene.instance()
	test_level.get_node("UserInterface").add_child(dialog_box)
	activator = activator_scene.instance()
	test_level.add_child(activator)

func after_each():
	test_level.free()

func test_next_dialog():
	dialog_box.dialog_index = 0
	dialog_box.dialog = ['aaaa', 'bbbb']
	dialog_box.load_next_dialog()
	assert_true(dialog_box.get_node("Background/Text").bbcode_text == dialog_box.dialog[0])
	dialog_box.load_next_dialog()
	assert_true(dialog_box.get_node("Background/Text").bbcode_text == dialog_box.dialog[1])

func test_activator():
	activator.emit_signal("body_entered", player)
	assert_true(activator.get_node("PopupBox").visible && activator.active)
	activator.emit_signal("body_exited", player)
	assert_true(!activator.get_node("PopupBox").visible && !activator.active)
