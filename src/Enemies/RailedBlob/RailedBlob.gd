#probably should extends enemies, but I decided to construct class enemy after
#we will know moore about enemies
extends Actor

var enemy_id
var enemy_velocity:Vector2
var cur_velocity:Vector2

#Blob will be snapped to cell's grid and can move only by steps on grid
#minimal step is size of map field, path hold one of for vectors:
# (0,1), (-1,0), (0,-1), (1,0) 
var path:Array
var path_iter
var step_time
var last_position:Vector2
var freeze_state
var freeze_frac = 0.1
var active
var dead = false
const PUSH_BACK = 4000
const ENEMY_HP = 80
func _ready():
	#for testing purpose we guard current_scene = null
	if(get_tree() != null):
		freeze_state = get_tree().get_current_scene().get_node("WorldState").get_node("Freeze")
	path_iter = 0
	step_time = 0.3
	last_position.x = self.position.x
	last_position.y = self.position.y
	active = true
	current_hp = ENEMY_HP
	

func _on_AttackField_body_entered(player):
	if(active == true):
		player.take_damage(30)
		player.push_away(self.position,enemy_velocity)


func _physics_process(delta):
	if (!dead):
		calculate_movement()
		cur_velocity = enemy_velocity
		if(freeze_state.get_freeze()):
			cur_velocity = freeze_frac * enemy_velocity
		move_and_slide(cur_velocity)
	

func set_id(id:int):
	enemy_id = id

func set_path(path_to_set:Array):
	path.clear()
	for i in range(0,path_to_set.size()):
		path += [path_to_set[i]]
	path_iter = 0

func print_type():
	return "RailedBlob"

func calculate_movement():
	
	var position_diff = 0
	position_diff += self.position.x - last_position.x
	position_diff += self.position.y - last_position.y
	if(position_diff >= 16 or position_diff <= -16 or position_diff == 0):
		last_position = position
		enemy_velocity = path[path_iter]
		path_iter = (path_iter + 1) % path.size()
		enemy_velocity = enemy_velocity * 16 / step_time
	
			

func _on_AreaChecker_area_entered(area):
	if area.is_in_group("Attack"):
		take_damage(area.get_parent().ATTACK_DAMAGE)
		push(area.get_parent().get_position())

#test
func push(player_pos):
	#set_modulate(Color(1.0, 0.3, 0.3, 0.3))
	if (self.position.x < player_pos.x):
		enemy_velocity.x -= PUSH_BACK
	else:
		enemy_velocity.x += PUSH_BACK
		
func die() -> void:
	active = false
	dead = true
	if (get_tree() != null):
		get_tree().get_current_scene().get_node("Player").get_point()
	queue_free()
