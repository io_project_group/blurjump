extends MarginContainer

onready var selector_one = $CenterContainer/VBoxContainer/CenterContainer/HBoxContainer/Selector
onready var selector_two = $CenterContainer/VBoxContainer/CenterContainer2/HBoxContainer/Selector
var current_selection
var active

func set_current_selection(choice):
	selector_one.text = ""
	selector_two.text = ""
	current_selection = choice
	if choice == 0:
		selector_one.text = ">"
	if choice == 1:
		selector_two.text = ">"

func _ready() -> void:
	set_current_selection(0)
	active = false

func _process(delta):
	if active:
		if Input.is_action_just_pressed("ui_down") and current_selection == 0:
			set_current_selection(1)
		if Input.is_action_just_pressed("ui_up") and current_selection == 1:
			set_current_selection(0)
		
		if Input.is_action_just_pressed("ui_accept"):
			if current_selection == 0:
				print("resume")
				get_parent().change_state()
			if current_selection == 1:
				print("menu")
				get_tree().paused = false
				get_tree().change_scene("res://src/Interface/MainMenu/MainMenu.tscn")
				
	
