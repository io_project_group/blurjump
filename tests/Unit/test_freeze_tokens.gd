extends "res://addons/gut/test.gd"

var test_level_scene = load("res://src/Levels/TestLevel.tscn")

var test_level
var player
var freeze_token

func before_each():
	test_level = test_level_scene.instance()
	player = test_level.get_node("Player")
	freeze_token = test_level.get_node("FreezeToken")

func after_each():
	test_level.free()

func test_taking_token():
	player._ready()
	player.current_freeze_tokens = 0
	player.MAX_FREEZE_TOKENS = 2
	freeze_token.get_node("PickUpArea").emit_signal("body_entered", player)
	assert_eq(player.current_freeze_tokens, 1)

func test_max_tokens():
	player._ready()
	player.current_freeze_tokens = player.MAX_FREEZE_TOKENS
	freeze_token.get_node("PickUpArea").emit_signal("body_entered", player)
	assert_eq(player.current_freeze_tokens, player.MAX_FREEZE_TOKENS)
