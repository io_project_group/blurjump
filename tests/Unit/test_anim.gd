extends "res://addons/gut/test.gd"

var loaded_scene = load("res://src/Player/Player.tscn")

var player_scene

func before_each():
	player_scene = loaded_scene.instance()

func after_each():
	player_scene.free()

# In this test we set character state to on flor, force jump and
# Check if variables that store time to specyfic accions have valid
# at the moment of jump and in next call - "simulated" frame after
func test_attack_anim():
	var character = player_scene.get_node(".")
	character.set_physics_process(false)
	character._ready()
	#set state on floor in player
	character.on_floor = true
	
	
	character.is_attacking = true
	character.attacking_anim = 0
	character.handle_move(0, 1)
	character.handle_attack(3) 
	assert_true(player_scene.get_node("HeroSprite").animation == "Attack1R",
			"Attack1R failed - Player.tscn")
			
	character.attacking_anim = 0
	character.handle_move(0, -1) 
	character.handle_attack(3) 
	assert_true(player_scene.get_node("HeroSprite").animation == "Attack1L",
			"Attack1L failed - Player.tscn")
			
	character.direction = true
	character.attacking_anim = 5
	character.handle_move(0, 1)
	character.handle_attack(2) 
	assert_true(player_scene.get_node("HeroSprite").animation == "Attack2R",
			"Attack2R failed - Player.tscn")
			
	character.attacking_anim = 5
	character.handle_move(0, -1) 
	character.handle_attack(2) 
	assert_true(player_scene.get_node("HeroSprite").animation == "Attack2L",
			"Attack2L failed - Player.tscn")

	character.attacking_anim = 5
	character.handle_move(0, 1) 
	character.handle_attack(1) 
	assert_true(player_scene.get_node("HeroSprite").animation == "Attack3R",
			"Attack3R failed - Player.tscn")
	
	character.attacking_anim = 5
	character.handle_move(0, -1) 
	character.handle_attack(1) 
	assert_true(player_scene.get_node("HeroSprite").animation == "Attack3L",
			"Attack3L failed - Player.tscn")
			
func test_movment_related_anim():
	var character = player_scene.get_node(".")
	character.set_physics_process(false)
	character._ready()
	#set state on floor in player
	character.on_floor = true
	
	character._ready()
	character.on_floor = true
	#Check If iddleR is played (delta = 0 => speed = 0)
	character.handle_move(0, 1)
	character.handle_animation(0) 
	assert_true(player_scene.get_node("HeroSprite").animation == "IddleR",
			"IddleR failed - Player.tscn")
		
	#Check If iddleL is played (delta = 0 => speed = 0)
	character.handle_move(0, -1) 
	character.handle_animation(0) 
	assert_true(player_scene.get_node("HeroSprite").animation == "IddleL",
			"IddleL failed - Player.tscn")
	
	#Check If moveR is played
	character.handle_move(0.1, 1) 
	character.handle_animation(0) 
	assert_true(player_scene.get_node("HeroSprite").animation == "WalkR",
			"WalkR failed - Player.tscn")
	
	character.player_velocity.x = 0
	
	#Check If moveL is played
	character.handle_move(0.1, -1) 
	character.handle_animation(0) 
	assert_true(player_scene.get_node("HeroSprite").animation == "WalkL",
			"WalkL failed - Player.tscn")
	
	character.player_velocity.x = 0
	character.handle_jump(0.1, true)
	character.handle_animation(0)  
	assert_true(player_scene.get_node("HeroSprite").animation == "JumpL",
			"JumpL failed - Player.tscn")
			
	character.player_velocity.x = 0
	character.player_velocity.y = 0
	character.handle_move(0.1, 1) 
	character.handle_jump(0.1, true)
	character.handle_animation(0) 
	
	assert_true(player_scene.get_node("HeroSprite").animation == "JumpR",
			"JumpR failed - Player.tscn")
	 
