extends "res://addons/gut/test.gd"

var Musketeer := load("res://src/Enemies/WalkingEnemy/WalkingEnemy.gd")

var _musketeer = null

func before_each():
	_musketeer = Musketeer.new()
	_musketeer._ready()

func after_each():
	_musketeer.free()

func test_push_away():
	_musketeer.position = Vector2(8.0, 10.0)
	_musketeer.push_away(Vector2(2.0, 2.0))
	assert_true(_musketeer.block_movement)
	assert_eq(_musketeer.push_away_mod, Vector2(0.6, 0.8) * _musketeer.PUSH_STRENGTH)

func test_change_block_flags():
	_musketeer.block_movement = true
	_musketeer.block_timer = -1
	_musketeer.change_block_flags(1.0)
	assert_false(_musketeer.block_movement)

func test_handle_push_eq_zero():
	_musketeer.push_away_mod = Vector2(0.0, 0.0)
	var x = _musketeer.handle_push(Vector2(1.0, 5.0))
	assert_eq(x, Vector2(1.0, 5.0))

func test_handle_push_same_direction():
	_musketeer.push_away_mod = Vector2(2.0, 1.0)
	var x = _musketeer.handle_push(Vector2(3.0, 0.0))
	assert_eq(x, Vector2(5.0, 1.0))

func test_handle_push_diffrent_direction():
	_musketeer.velocity = Vector2(-4.0, 1.0)
	_musketeer.push_away_mod = Vector2(2.0, 1.0)
	var x = _musketeer.handle_push(Vector2(-2.0, 0.0))
	assert_eq(_musketeer.velocity.x, 0.0)
	assert_eq(x, Vector2(2.0, 1.0))
