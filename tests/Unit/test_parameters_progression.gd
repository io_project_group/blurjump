extends "res://addons/gut/test.gd"

var test_level_scene = load("res://src/Levels/TestLevel.tscn")

var test_level
var player
var HPtimer
var MPtimer

func before_each():
	test_level = test_level_scene.instance()
	player = test_level.get_node("Player")
	HPtimer = test_level.get_node("Player/HPTimer")
	MPtimer = test_level.get_node("Player/MPTimer")
	
func after_each():
	test_level.free()
	
func test_hp_progression():
	player._ready()
	player.current_hp = 1
	yield(get_tree().create_timer(2.5), "timeout")
	var timer = player.get_node("HPTimer")
	assert_true(timer.time_left == 0)
	timer.emit_signal("timeout")
	assert_true(player.current_hp > 1)
	
func test_mp_progression():
	player._ready()
	player.current_mp = 1
	yield(get_tree().create_timer(3.4), "timeout")
	var timer = player.get_node("MPTimer")
	assert_true(timer.time_left == 0)
	timer.emit_signal("timeout")
	assert_true(player.current_hp > 1)
	
	
