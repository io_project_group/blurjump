extends "res://addons/gut/test.gd"

var Blur := load("res://src/Blur/blur.gd")

var _blur = null

func before_each():
	_blur = Blur.new()

func after_each():
	_blur.free()

func test_init():
	assert_eq(_blur.life_length, 0.0)


func test_set_position_zero():
	_blur.set_to_player(Vector2(0.0, 0.0), true)
	assert_true(_blur.position.x == _blur.distance.x)
	assert_true(_blur.position.y == _blur.distance.y)

func test_set_position_not_zero():
	_blur.set_to_player(Vector2(123.0, 45.0), true)
	assert_true(_blur.position.x == 123.0 + _blur.distance.x)
	assert_true(_blur.position.y == 45.0 + _blur.distance.y)

func test_move_right():
	_blur.set_to_player(Vector2(0.0, 0.0), true)
	assert_true(_blur.blur_speed.x >= 0.0)
	
func test_move_left():
	_blur.set_to_player(Vector2(0.0, 0.0), false)
	assert_true(_blur.blur_speed.x <= 0.0)
	
func test_freeze_time_true():
	_blur.is_freeze = true
	var freeze_state = preload("res://src/WorldState/Freeze.gd")
	_blur.freeze_state = freeze_state.new()
	var EPS = _blur.freeze_state.get_eps()
	var vec = _blur.blur_speed / EPS
	_blur.freeze_velocity = vec
	_blur.freeze_param = EPS
	_blur.freeze_time()
	assert_eq(_blur.blur_speed, _blur.freeze_velocity)
	_blur.freeze_state.free()
	
func test_freeze_time_false():
	_blur.is_freeze = false
	var freeze_state = preload("res://src/WorldState/Freeze.gd")
	_blur.freeze_state = freeze_state.new()
	var EPS = _blur.freeze_state.get_eps()
	var vec = _blur.blur_speed / EPS
	_blur.freeze_velocity = vec
	_blur.freeze_param = EPS
	_blur.freeze_time()
	assert_eq(_blur.blur_speed, _blur.freeze_velocity * EPS)
	_blur.freeze_state.free()
	
