extends Node

const EPS = 5
var freeze_active = false
var _player

const DURATION = 2
var is_freeze_active
var time_start
var duration_ms
var freeze_start


func _ready():
	_player = get_parent().get_parent().get_node("Player")	
	_player.connect("freeze_start", self, "_freeze_start")

func _physics_process(delta):
	freeze_time()
	
func freeze_time():
	if freeze_active:
		var curr_time = OS.get_ticks_msec() - time_start
		if curr_time >= duration_ms:
			freeze_active = false
			
func _freeze_start():
	time_start = OS.get_ticks_msec()
	duration_ms = DURATION * 1000
	freeze_active = true

func get_freeze():
	return freeze_active	
	
func get_eps():
	return EPS


