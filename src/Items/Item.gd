extends KinematicBody2D
class_name Item

export var DURATION = 5.0
export var SPEED: = Vector2(50.0, 100.0)
export var GRAVITY: = 250.0

onready var is_on_floor = false
onready var velocity = SPEED

func _physics_process(delta: float) -> void:
	if !is_on_floor:
		velocity.y += GRAVITY * delta
		velocity.y = move_and_slide(velocity, Vector2.UP).y
		if is_on_floor():
			is_on_floor = true
			velocity = Vector2.ZERO

func take_effect(player):
	pass

	
func _on_PickUpArea_body_entered(player: Player) -> void:
	self.take_effect(player)
