extends Actor

class_name Raven

enum {FLYING, ATTACKING, TURNING, CIRCLING}

export var ATTACK_DAMAGE = 10
export var ATTACK_RANGE = 100
export var FLYING_SPEED = Vector2(200.0, 0)
export var ATTACK_SPEED = 250.0
export var TURNING_FACTOR = 2 # defines how far is turning point

onready var player = get_parent().get_node("Player")
onready var state = FLYING
onready var velocity = -scale * FLYING_SPEED
onready var target = Vector2.ZERO
onready var pushed = false

var creator


func _physics_process(delta: float) -> void:
	velocity.y = move_and_slide(velocity).y
	match state:
		FLYING:
			handle_flying()
		ATTACKING:
			handle_attacking()
		TURNING:
			handle_turning()
		CIRCLING:
			handle_circling()

func handle_flying():
	if (abs(player.position.x - self.position.x) < ATTACK_RANGE):
		state = ATTACKING
		start_attacking()

func handle_attacking():
	if (target_reached()):
		state = TURNING
		# when attacking from below do not reflect velocity.y
		velocity.y *= -1 if velocity.y > 0 else 1
		target = target + velocity * TURNING_FACTOR
		rotate_to_target()

func handle_turning():
	if (target_reached()):
		state = ATTACKING
		start_attacking()
		rotate_to_target()

func handle_circling():
	pass

func start_attacking():
	target = player.global_position
	var direction_vector = (target - self.global_position).normalized()
	velocity = ATTACK_SPEED * direction_vector
	rotate_to_target()

# Checks if raven already passed the target.
func target_reached():
	if (pushed):
		pushed = false
		return true
	return sign(target.x - self.global_position.x) != sign(velocity.x)
	
func rotate_to_target():
	$RavenSound.play()
	self.rotation =  Vector2.LEFT.angle_to(velocity) if scale.x > 0 else Vector2.RIGHT.angle_to(velocity)
	scale.y = sign(cos(rotation))
	
func push_away():
	pushed = true

func die():
	if (creator != null):
		creator.raven_died()
	queue_free()

func _on_AttackArea_entered(body: Node) -> void:
	if (body == player):
		body.take_damage(ATTACK_DAMAGE)

func _on_DamageArea_area_entered(area: Area2D) -> void:
	if (area.is_in_group("Attack")):
		take_damage(area.get_parent().ATTACK_DAMAGE)
		
func get_class():
	return "Raven"
