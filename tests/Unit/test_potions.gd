extends "res://addons/gut/test.gd"

var test_level_scene = load("res://src/Levels/TestLevel.tscn")

var test_level
var player
var health_potion
var energy_potion
var attack_power_potion

func before_each():
	test_level = test_level_scene.instance()
	player = test_level.get_node("Player")
	health_potion = test_level.get_node("HealthPotion")
	energy_potion = test_level.get_node("EnergyPotion1")
	attack_power_potion = test_level.get_node("AttackPowerPotion")

func after_each():
	test_level.free() 

func test_health_potion():
	player._ready()
	assert(player.current_hp == player.HP)
	player.current_hp -= health_potion.HP_RESTORE_VALUE
	health_potion.get_node("PickUpArea").emit_signal("body_entered", player)
	assert_true(player.current_hp == player.HP)

func test_attack_power_potion():
	player._ready()
	var prev_dmg = player.ATTACK_DAMAGE
	attack_power_potion.BOOST_TIME = 0.5
	Timer
	attack_power_potion.get_node("PickUpArea").emit_signal("body_entered", player)
	assert_true(player.ATTACK_DAMAGE == prev_dmg * attack_power_potion.ATTACK_BOOST_FACTOR)
	yield(get_tree().create_timer(attack_power_potion.BOOST_TIME), "timeout")
	var timer = attack_power_potion.get_node("AttackPowerTimer")
	assert_true(timer.time_left == 0)
	timer.emit_signal("timeout")
	assert_true(player.ATTACK_DAMAGE == prev_dmg)
	
func test_energy_potion():
	player._ready()
	assert(player.current_mp == player.MP)
	player.current_mp -= energy_potion.MP_RESTORE_VALUE
	energy_potion.get_node("PickUpArea").emit_signal("body_entered", player)
	assert_true(player.current_mp == player.MP)
