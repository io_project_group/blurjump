extends Area2D

export var dialog = "0"

onready var dialog_box = preload("res://src/Interface/Dialogs/DialogBox.tscn")
onready var active = false

func _ready() -> void:
	$PopupBox.visible = false
	
func _process(delta: float) -> void:
	if active and Input.is_action_just_pressed("Interact"):
		var box = dialog_box.instance()
		var dialog_file = File.new()
		dialog_file.open("res://dialogs/tutorial_messages.json", File.READ)
		var json = JSON.parse(dialog_file.get_as_text())
		dialog_file.close()
		box.dialog = json.result[dialog]
		get_tree().get_current_scene().get_node('UserInterface').add_child(box)

func _on_DialogActivationArea_body_entered(body: Node) -> void:
	if body.get_class() == 'Player':
		$PopupBox.visible = true
		active = true

func _on_DialogActivationArea_body_exited(body: Node) -> void:
	if body.get_class() == 'Player':
		$PopupBox.visible = false
		active = false
