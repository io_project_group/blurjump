extends Area2D

export var SPEED := 500.0
var target := Vector2()
var movement_vector := Vector2()
var damage
var life_length

func _ready() -> void:
	life_length = 5.0

func _physics_process(delta: float) -> void:
	position += movement_vector * SPEED * delta
	life_length -= delta
	if (life_length <= 0):
		queue_free()

func set_target_and_damage(target_position: Vector2, start_position: Vector2, enemy_damage: int):
	movement_vector = (target_position - start_position).normalized()
	self.position = start_position
	damage = enemy_damage


func _on_Area2D_area_entered(area: Area2D) -> void:
	if (area.get_class() == "TileMap" or area.is_in_group("Shield")):
		queue_free()

func _on_Area2D_body_entered(body: Node) -> void:
	if (body.get_class() == "Player"):
		body.take_damage(damage)
		body.push_away(self.position, movement_vector)
	queue_free()
