extends "res://addons/gut/test.gd"

var test_scene = load("res://tests/Testerka.tscn")
var raven_master_scene = load("res://src/Enemies/RavenMaster/RavenMaster.tscn")
var test_level_scene = load("res://src/Levels/TestLevel.tscn")

var test
var raven_master
var test_level

func before_each():
	test = test_scene.instance()
	test.add_child(test_level_scene.instance())
	test_level = test.get_node("TestLevel")
	raven_master = test.get_node("TestLevel/RavenMaster")

func after_each():
	test.free()

func test_die():
	raven_master.current_hp = raven_master.HP
	raven_master.take_damage(raven_master.HP) # take max damage
	assert_true(raven_master.current_hp == 0)

func test_spawn_max():
	raven_master._ready()
	for i in range(0, raven_master.MAX_RAVENS_NUM):
		raven_master.spawn_raven()
	assert_true(raven_master.ravens_num == raven_master.MAX_RAVENS_NUM)
	
func test_raven_died():
	raven_master.ravens_num = 1
	raven_master.raven_died()
	assert_true(raven_master.ravens_num == 0)

func test_cooldown():
	raven_master.cooldown_left = 1.0
	raven_master.reduce_cooldown(1.0)
	assert_true(raven_master.cooldown_left == 0)
	raven_master.reduce_cooldown(1.0)
	assert_true(raven_master.cooldown_left == 0)
