extends CanvasLayer
class_name Hud

#hud need to be children of node that hold player class
var player_ref
var last_hp
var last_mp
var last_tokens
var max_hp
var max_mp
var max_tokens
var is_hp_full
var is_mp_full
onready var bar_pixel_width = 86
onready var mark_offset = 8

func _ready():
	if(player_ref == null):
		player_ref = get_parent().get_node("Player")
	last_hp = player_ref.HP
	last_mp = player_ref.MP
	last_tokens = player_ref.START_FREEZE_TOKENS
	max_hp = player_ref.HP
	max_mp = player_ref.MP
	max_tokens = player_ref.MAX_FREEZE_TOKENS
	is_hp_full = true;
	is_mp_full = true;
	update_HP()
	update_MP()
	update_tokens()

#to wszystko są wirujące blokady - ale niedopilnowaliśmy, żeby zmiana hp gracza
#szła przez jedną funkcję więc chyba już inaczej się nie da
func _physics_process(delta):
	if(player_ref.current_hp != last_hp):
		last_hp = player_ref.current_hp
		update_HP()
	if(player_ref.current_mp != last_mp):
		last_mp = player_ref.current_mp
		update_MP()
	if(player_ref.current_freeze_tokens != last_tokens):
		last_tokens = player_ref.current_freeze_tokens
		update_tokens()

# let p - parameter
#standard bar should change is_hp/mp_full so it return proper result
func standard_bar(p, max_p, is_p_full, name, type):
	var cap_r_name = type + "CapR"
	var cap_l_name = type + "CapL"
	var mark_name = type + "Mark"
	var ret_bool = is_p_full
	
	#right filling of bar
	if(p != max_p and is_p_full == true):
		ret_bool = false
		get_node(name).get_node(cap_r_name).visible = false;
		get_node(name).get_node(mark_name).visible = true;	
	if(p == max_p and  is_p_full == false):
		ret_bool = true
		get_node(name).get_node(cap_r_name).visible = true;
		get_node(name).get_node(mark_name).visible = false;
	
	#left filling of bar
	if(p <= 0):
		get_node(name).get_node(cap_l_name).visible = false;
	
	#handling main bar
	var ratio = p * bar_pixel_width / max_p
	get_node(name).get_node(type).scale.x = -ratio
	var mark_position = ratio + mark_offset
	print(mark_position)
	get_node(name).get_node(mark_name).offset.x = mark_position
	
	return ret_bool
		
func update_HP():
	is_hp_full = standard_bar(last_hp, max_hp, is_hp_full, "Health", "HP")
	
func update_MP():
	is_mp_full = standard_bar(last_mp, max_mp, is_mp_full, "Mana", "MP")
		
func update_tokens():
	var name = "Token"
	var iter_name
	for i in range(1,6):
		iter_name = name + str(i)
		if(i <= last_tokens):
			get_node("Tokens").get_node(iter_name).visible = true
		else:
			get_node("Tokens").get_node(iter_name).visible = false
