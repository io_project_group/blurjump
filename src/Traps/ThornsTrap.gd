extends Area2D

export var DAMAGE = 30

# Time in secs. After that time if player didn't left trap area 
# it will take damage once again.
export var COOLDOWN = 1

func _on_trap_entered(actor: Actor) -> void:
	var path = actor.get_path()
	actor.take_damage(DAMAGE)
	if !get_node(path):
		return
	actor.is_taking_damage = true
	deal_damage(actor, path)
	
func _on_trap_exited(actor: Actor) -> void:
	actor.is_taking_damage = false

func deal_damage(actor: Actor, path: String):
	while(get_node(path) && actor.is_taking_damage):
		yield(get_tree().create_timer(COOLDOWN), "timeout")
		if actor and actor.is_taking_damage:
			actor.take_damage(DAMAGE)
