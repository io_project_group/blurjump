extends "res://addons/gut/test.gd"

var player_scene = load("res://src/Player/Player.tscn")
var thorns_trap_scene = load("res://src/Traps/ThornsTrap.tscn")
var slowing_trap = load("res://src/Traps/SlowingTrap.tscn")

var _player_scene = null
var _thorns_trap_scene = null
var _slowing_trap = null

var player

func before_each():
	_player_scene = player_scene.instance()
	_thorns_trap_scene = thorns_trap_scene.instance()
	_slowing_trap = slowing_trap.instance()
	player = _player_scene.get_node(".")

func after_each():
	_player_scene.free()
	_thorns_trap_scene.free()
	_slowing_trap.free()
	

func test_thorns_damage():
	var thorns_trap = _thorns_trap_scene.get_node(".")
	var player_path = player.get_path()
	assert_true(player_path != null) # player is alive before
	player._ready()
	thorns_trap.emit_signal("body_entered", player)
	thorns_trap.emit_signal("body_exited", player)
	if player.HP < thorns_trap.DAMAGE:
		assert_true(_player_scene.get_node(player_path) == null) # player is dead
	else:
		assert_true(player.current_hp < player.HP)
	
func test_thorns_cooldown():
	var thorns_trap = _thorns_trap_scene.get_node(".")
	var player_path = player.get_path()
	assert_true(player_path != null) # player is alive before
	player.current_hp = player.HP
	thorns_trap.emit_signal("body_entered", player)
	var wait_time = thorns_trap.COOLDOWN * (player.HP / thorns_trap.DAMAGE)
	yield(get_tree().create_timer(wait_time), "timeout")
	assert_true(_player_scene.get_node(player_path) == null) # player should be dead
	
func test_slowing():
	var speed_before = player.SPEED_MAX
	var jump_speed_before = player.JUMP_SPEED
	var slow_factor = _slowing_trap.SLOW_FACTOR
	_slowing_trap.emit_signal("body_entered", player)
	assert_true(player.SPEED_MAX == speed_before / slow_factor)
	assert_true(player.JUMP_SPEED == jump_speed_before / slow_factor)
	_slowing_trap.emit_signal("body_exited", player)
	assert_true(player.SPEED_MAX == speed_before)
	assert_true(player.JUMP_SPEED == jump_speed_before)
