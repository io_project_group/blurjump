extends StaticBody2D

export var blur_speed := Vector2(45.0, 0.0)
export var distance := Vector2(16.0, 0.0)
export var MAX_LIFE_LENGTH = 5.0

var life_length
var is_moving_right

var freeze_velocity
var is_freeze
var freeze_state
var freeze_param
func _ready():
	freeze_state = get_tree().get_current_scene().get_node("WorldState").get_node("Freeze")
	is_freeze = freeze_state.get_freeze()
	freeze_param = freeze_state.get_eps()
	freeze_velocity = blur_speed / freeze_param

func _init() -> void:
	life_length = 0.0
	is_moving_right = true
	
func die() -> void:
	if life_length > MAX_LIFE_LENGTH:
		queue_free()

func _physics_process(delta: float) -> void:
	is_freeze = freeze_state.get_freeze()
	freeze_time()
	life_length += delta
	position += blur_speed * delta;
	die()

func set_to_player(player_position: Vector2, spawn_right: bool):
	position.y = player_position.y
	if spawn_right:
		position.x = player_position.x + distance.x
	else:
		is_moving_right = false
		blur_speed.x *= -1
		position.x = player_position.x - distance.x
	position.y += distance.y
	
func freeze_time():
	if is_freeze:
		blur_speed = freeze_velocity
	else:
		blur_speed = freeze_velocity * freeze_param
