#EnemyMenager should only have child that are enemy
extends Node

var curr_id_nr

func _ready():
	curr_id_nr = 1
	for enemy in self.get_children():
		enemy.enemy_id = curr_id_nr
		prepare(enemy)
		curr_id_nr += 1

func prepare(enemy):
	
	#path_nr let hiding path in editor  
	var path_nr = 0
	var path:Array
	
	if(enemy.print_type() == "RailedBlob"):
		if(path_nr == 0):
			for i in range(6): 
				path += [Vector2(1,0)]
			for i in range(3): 
				path += [Vector2(0,1)]
			for i in range(11): 
				path += [Vector2(-1,0)]
			for i in range(3): 
				path += [Vector2(0,-1)]
			for i in range(5): 
				path += [Vector2(1,0)]
			enemy.set_path(path)

