extends "res://src/Blur/blur.gd"

export(String, "center", "side_to_side") var animation
export var animation_length = 1.0

func die() -> void:
	#Usunięto kurczenie się bluru
	#if life_length > MAX_LIFE_LENGTH - animation_length:
	#	if animation == "center":
	#		$AnimationCenter.play("blurplatform_center_slide", -1,
	#		1.0 / animation_length) # animation length inversely proportional to animation speed
	#	else:
	#		if !is_moving_right:
	#			$AnimationCenter.play("blurplatform_left_to_right_slide", -1,
	#			 1.0 / animation_length) # animation length inversely proportional to animation speed
	#		else:
	#			$AnimationCenter.play("blurplatform_right_to_left_slide", -1,
	#			 1.0 / animation_length) # animation length inversely proportional to animation speed
	if life_length > MAX_LIFE_LENGTH:
		queue_free()
func _physics_process(delta: float) -> void:
	handle_anim()
	
func handle_anim():
	if(life_length > 0.15 and life_length < 0.22):
		if(is_moving_right):
			$BlurSprite.play("BlurSpawnR")
			$CollisionShape2D.position.x = 3
		else:
			$BlurSprite.play("BlurSpawnL")
			$CollisionShape2D.position.x = -3
			$BlurSprite.offset.y = 6
		
	if(life_length > 0.22):
		$BlurSprite.offset.x = 0
		$BlurSprite.offset.y = 0
		if(is_moving_right):
			$BlurSprite.play("BlurMoveR")
		else:
			$BlurSprite.play("BlurMoveL")
	if(life_length > MAX_LIFE_LENGTH/2):
		var tLin = life_length - MAX_LIFE_LENGTH/2
		$BlurSprite.modulate.a = (1-2*tLin/MAX_LIFE_LENGTH)
		
