extends Node2D

func _ready():
	get_node("RavenMaster").set_physics_process(false)

func _on_RavenActivateSwitch_body_entered(body):
	if body.get_class() == 'Player':
		if get_tree().get_current_scene().has_node("RavenMaster"):
			$RavenMaster.set_physics_process(true)

func _on_Win_body_entered(body):
	if body.get_class() == 'Player':
		print("Lvl1 Ends. You won!")
		get_tree().change_scene("res://src/Interface/MainMenu/MainMenu.tscn")
